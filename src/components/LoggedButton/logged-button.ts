import { defineComponent } from 'vue'
import { mapActions, mapGetters } from 'vuex'

export default defineComponent({
  name: 'LoggedButton',
  computed: {
    ...mapGetters({
      logged: 'Auth/getLogged',
    }),
  },
  methods: {
    ...mapActions({
      login: 'Auth/login',
      logout: 'Auth/logout',
    }),
    onClick() {
      this.logged ? this.logout() : this.login()
    },
  },
})
