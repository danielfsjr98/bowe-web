import { defineComponent } from 'vue'
import { mapActions, mapMutations } from 'vuex'
import Navbar from '../Navbar/index.vue'

export default defineComponent({
  name: 'TemplateProvider',
  components: { Navbar },

  methods: {
    ...mapActions({
      validateToken: 'Auth/validateToken',
    }),
  },
  mounted() {
    this.validateToken()
  },
})
