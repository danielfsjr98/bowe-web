import { defineComponent } from 'vue'

export default defineComponent({
  name: 'CalculatorCard',
  props: {
    icon: String,
    text: String,
    onClick: {
      type: Function,
      required: true,
    },
    selected: Boolean,
  },
})
