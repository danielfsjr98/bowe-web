import { defineComponent } from 'vue'

const items = [
  {
    path: '/',
    name: 'Início',
    icon: 'bi-house-fill',
  },
  {
    path: '/',
    hash: '#calculator',
    name: 'Calculadora',
    icon: 'bi-calculator-fill',
  },
  {
    path: '/',
    hash: '#faq',
    name: 'FAQ',
    icon: 'bi-question-circle-fill',
  },
  {
    path: '/about',
    name: 'Sobre',
    icon: 'bi-info-circle-fill',
  },
]

import LoggedButton from '../LoggedButton/index.vue'
import * as bootstrap from 'bootstrap'

export default defineComponent({
  name: 'Navbar',
  components: { LoggedButton },
  data: () => ({
    items,
  }),
  methods: {
    goToHome() {
      this.$router.push('/')
    },
    isActiveRoute(item: { path: string; hash?: string }) {
      const fullPath = item.path + (item.hash || '')

      if (fullPath === '/') {
        return this.$route.fullPath === fullPath
      }

      return this.$route.fullPath.includes(fullPath)
    },
  },
  mounted() {
    const navLinks = document.querySelectorAll('.item-container')
    const menuToggle = document.getElementById('n_bar')
    navLinks.forEach(l => {
      l.addEventListener('click', () => {
        new bootstrap.Collapse(menuToggle as any).toggle()
      })
    })
  },
})
