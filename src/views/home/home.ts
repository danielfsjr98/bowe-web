import { defineComponent } from 'vue'
import CalculatorCard from '../../components/CalculatorCard/index.vue'

export default defineComponent({
  data: () => ({
    hiringForm: {
      name: '',
      email: '',
      phone: ''
    },
    calculateForm: {
      type: 'residential',
      month: '',
      consumption: null,
      consumptionHP: null,
      consumptionHFP: null,
      state: '',
      city: '',
      dealership: '',
      connectionType: '',
    },
    faq: [
      {
        title: 'Como funciona?',
        content: `          
          <p class="my-3">
            Os créditos de energia que nossa usina gera são enviados para sua conta da Distribuidora, onde o valor desses créditos será abatido da sua conta de luz, ou seja, não será cobrado pela Distribuidora.
            <br/><br/>
            A partir disso, nós validamos o quanto dessa energia foi necessitada através do seu consumo e colocamos um desconto de 15% na tarifa que você teria pago à Distribuidora.
            <br/><br/>
            No caso, você continuará pagando para a Distribuidora a iluminação pública, impostos e o consumo mínimo, itens os quais não conseguimos atuar sobre.
            <br/><br/>
            Todos os meses nós analisamos como está o seu consumo de energia e adequamos a quantidade de energia que você irá receber, visando aumentar o seu desconto percebido!
          </p>
        `,
      },
      {
        title: 'E como vocês sabem quanto eu gastei de energia?',
        content: `          
          <p class="my-3">
            Todo mês a concessionária faz a leitura do seu consumo de energia normalmente, mas agora sua conta virá com um item referente à energia que nós enviamos para você e que foi abatido da sua conta concessionária, que é a energia injetada.
            <br/><br/>
            É nesse valor que nós aplicamos o desconto de 15%, ou seja, o desconto é sobre a tarifa da energia.
          </p>
        `,
      },
      {
        title: 'E se por acaso meu consumo for MAIOR que o injetado por vocês?',
        content: `          
          <p class="my-3">
            Você paga a energia normalmente para a concessionária.
            <br/><br/>
            Exemplo: Você consumiu 1000 kWh no mês e nós injetamos no seu relógio 900kWh. Você pagará a diferença, os 100kWh para a Distribuidora normalmente.
          </p>
        `,
      },
      {
        title: 'E se por acaso meu consumo for MENOR que o injetado por vocês?',
        content: `          
          <p class="my-3">
            Você paga somente pelo que realmente usar.
            <br/><br/>
            Caso você tenha recebido mais energia do que usou, a energia restante virará créditos que poderão ser utilizados posteriormente. Nós somente iremos cobrar por esses créditos quando você realmente os utilizar.
          </p>
        `,
      },
      {
        title: 'Então, como ficam os pagamentos?',
        content: `          
          <p class="my-3">
            Você receberá apenas uma fatura de energia, da Bow-e. Nós ficamos responsáveis pelo pagamento de sua conta com a Distribuidora.
            <br/><br/>
            Na fatura da Bow-e, será cobrado o valor com desconto da energia compensada de sua unidade, ou seja, a energia que você utilizou da geração de nossa usina e que foi abatida da sua conta da distribuidora.
          </p>
        `,
      },
      {
        title: 'Existe algum custo para adesão?',
        content: `          
          <p class="my-3">
            Não! Para aderir ao desconto na tarifa é necessário realizar somente a assinatura dos contratos, sem nenhum custo.
            <br/><br/>
            Você só pagará a fatura da Bow-e após a utilização da energia gerada por nossa usina, ou seja, somente quando você receber o desconto!
          </p>
        `,
      },
      {
        title: 'Como é feito o cálculo do meu desconto?',
        content: `          
          <p class="my-3">
            O desconto de 15% é aplicado sobre a tarifa de energia no respectivo ciclo de faturamento.
            <br/><br/>
            Por exemplo, se o seu consumo em determinado mês for de 5000 kWh e a energia compensada por nós foi de 4800 kWh, o desconto de 15% será aplicado sobre os 4800 kWh que enviamos e os outros 200 kWh serão cobrados com seu valor integral pela própria distribuidora de energia. A Bow-e só cobrará pelo que você realmente utilizou da energia gerada.
            <br/><br/>
            Além disso, não é possível aplicar nenhum tipo de desconto em alguns custos da conta de energia da distribuidora tais como taxa de iluminação pública, impostos, doações, multas etc.
          </p>
        `,
      },
      {
        title: 'E quanto tempo de contrato tenho de ter com vocês?',
        content: `          
          <p class="my-3">
            Além do ótimo desconto que lhe fornecemos, nosso diferencial é NÃO TER FIDELIDADE, justamente porque confiamos no nosso trabalho e não precisamos te amarrar em um contrato longo.
            <br/><br/>
            Em caso de necessidade de encerramento de contrato, pedimos apenas que nos avisem em um prazo de 90 dias de antecedência, pois é o tempo que a concessionária tem para descadastrar sua unidade conforme as leis vigentes.
          </p>
        `,
      },
      {
        title: 'O que muda na minha relação com a distribuidora de energia?',
        content: `          
          <p class="my-3">
            Sua relação com a distribuidora de energia local permanece a mesma.
            <br/><br/>
            Ao se tornar cliente da Bow-e você continuará sendo um cliente da sua distribuidora de energia local, ou seja, não há risco de descontinuidade no fornecimento de energia.
          </p>
        `,
      },
      {
        title: 'Quem autoriza e fiscaliza a Bow-e Energy?',
        content: `          
          <p class="my-3">
            A Bow-e opera com sistemas de Geração Distribuída regulamentado pela Resolução Normativa ANEEL nº 482/2012, Resolução Normativa nº 687/2015 e Resolução Normativa Nº.414/2010. Dessa forma estamos em conformidade com a ANEEL e cumprimos mensalmente as exigências das distribuidoras de energia.
          </p>
        `,
      },
    ],
  }),
  name: 'HomePage',
  components: { CalculatorCard },
  methods: {
    toggleType(type: string) {
      this.calculateForm.type = type
    },
    submitCalculate() {
      console.log(this.calculateForm)
    },
    submitHiring() {
      console.log(this.hiringForm)
    },
    addValidationFunction() {
      // Fetch all the forms we want to apply custom Bootstrap validation styles to
      const forms = document.querySelectorAll('.needs-validation')

      // Loop over them and prevent submission
      Array.prototype.slice.call(forms).forEach(function (form) {
        form.addEventListener(
          'submit',
          function (event: any) {
            if (!form.checkValidity()) {
              event.preventDefault()
              event.stopPropagation()
            }

            form.classList.add('was-validated')
          },
          false
        )
      })
    },
  },
  mounted() {
    this.addValidationFunction()
  },
})
