import { RouteRecordRaw } from 'vue-router'

export const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'Home',
    component: () => import('../views/home/index.vue'),
  },
  {
    path: '/about',
    name: 'About',
    component: () => import('../views/about/index.vue'),
  },
  {
    path: '/:pathMatch(.*)*',
    name: 'Not found',
    component: () => import('../components/NotFound/index.vue'),
  },
]
