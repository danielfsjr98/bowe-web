import Vue from 'vue'
import { ActionContext } from 'vuex'
import { BOWE_LOCAL_STORAGE_TOKEN, EXP_TOKEN } from '../../constants'

export default {
  async login({ commit }: ActionContext<any, any>) {
    commit('SET_LOGGED', true)
    localStorage.setItem(
      BOWE_LOCAL_STORAGE_TOKEN,
      new Date().valueOf().toString()
    )
  },
  async logout({ commit }: ActionContext<any, any>) {
    commit('SET_LOGGED', false)
    localStorage.removeItem(BOWE_LOCAL_STORAGE_TOKEN)
  },
  async validateToken({ commit }: ActionContext<any, any>) {
    const token = localStorage.getItem(BOWE_LOCAL_STORAGE_TOKEN)
    const result = !!token && parseInt(token) + EXP_TOKEN > new Date().valueOf()

    if (!result) {
      localStorage.removeItem(BOWE_LOCAL_STORAGE_TOKEN)
      commit('SET_LOGGED', false)
      return
    }
    commit('SET_LOGGED', true)
  },
}
