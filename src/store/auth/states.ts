export interface IAuthStates {
  logged: boolean
}

const states: IAuthStates = {
  logged: true,
}

export default states
