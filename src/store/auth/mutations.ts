import { IAuthStates } from './states'

export default {
  SET_LOGGED(state: IAuthStates, _payload: boolean) {
    state.logged = _payload
  },
}
