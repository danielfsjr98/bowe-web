import { IAuthStates } from './states'

export default {
  getLogged(state: IAuthStates) {
    return state.logged
  },
}
