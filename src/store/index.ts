import Vuex from 'vuex'
import Auth from './auth/index'

const store = new Vuex.Store({
  modules: {
    Auth,
  },
})

export default store
